#ifndef CUSTOMWIDGET_H
#define CUSTOMWIDGET_H

#include <QWidget>
#include <QSlider>
#include <QtCharts>
#include <QCheckBox>
#include <valarray>

class CustomWidget : public QWidget
{
    Q_OBJECT

public:
    CustomWidget();
    ~CustomWidget();

    void initLayout();
    void updateWidget();
    void calculateCompoundInterest(double, int, double, double, std::valarray<double> &, std::valarray<double> &, std::valarray<double> &);

public slots:
    void updateYears();
    void updateMonthlyAdd();
    void updateInterest();
    void updateStartVal();
    void updateCheckBoxMTot(int);
    void updateCheckBoxMIT(int);
    void updateCheckBoxMIC(int);
    void updateCheckBoxPrincipal(int);

private:
    int     m_numberOfYears;
    int     m_monthlyAdd;
    double  m_yearlyInterestPct;
    double  m_startVal;

    std::valarray<double> m_quarterlyTotal;
    std::valarray<double> m_quarterlyInterestTotal;
    std::valarray<double> m_quarterlyInterestCumul;

    QSlider *m_yearSlider;
    QSlider *m_monthlyAddSlider;
    QSlider *m_interestSlider;
    QSlider *m_startValSlider;

    QLCDNumber *m_yearLCD;
    QLCDNumber *m_monthlyAddLCD;
    QLCDNumber *m_interestLCD;
    QLCDNumber *m_startLCD;

    QLabel *m_yearLabel;
    QLabel *m_monthlyAddLabel;
    QLabel *m_interestLabel;
    QLabel *m_startLabel;

    QCheckBox *m_checkboxMonthlyTotal;
    QCheckBox *m_checkboxMonthlyInterestTotal;
    QCheckBox *m_checkboxMonthlyInterestCumul;
    QCheckBox *m_checkboxPrincipal;

    QtCharts::QBarSet *m_quarterlyTotalSet;
    QtCharts::QBarSet *m_quarterlyInterestSet;
    QtCharts::QBarSet *m_quarterlyInterestCumulSet;
    QtCharts::QBarSet *m_quarterlyPrincipalSet;

    QtCharts::QStackedBarSeries *m_QStackedBarSeries;
    QtCharts::QChart *m_chart;
    QtCharts::QValueAxis *m_axisX;
    QtCharts::QValueAxis *m_axisY;
    QtCharts::QChartView *m_chartView;
    QMainWindow m_window;

};

#endif // CUSTOMWIDGET_H
