#include <iostream>
#include <vector>
#include "CustomWidget.h"

#include <QApplication>


int main(int argc, char *argv[]) {

    QApplication app(argc, argv);

    CustomWidget custWidget;

    return app.exec();
}
