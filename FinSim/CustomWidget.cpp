#include "CustomWidget.h"
#include <iostream>

CustomWidget::CustomWidget() : QWidget(), m_numberOfYears(15), m_monthlyAdd(800), m_yearlyInterestPct(5), m_startVal(0) {

    this->setGeometry(500,250,1000,500);

    m_quarterlyTotal.resize(m_numberOfYears * 4);
    m_quarterlyInterestTotal.resize(m_numberOfYears * 4);
    m_quarterlyInterestCumul.resize(m_numberOfYears * 4);

    // Init the checkboxes
    m_checkboxMonthlyTotal = new QCheckBox("Quarterly Total",this);
    m_checkboxMonthlyInterestTotal = new QCheckBox("Quarterly Interest",this);
    m_checkboxMonthlyInterestCumul = new QCheckBox("Cumulated Interest",this);
    m_checkboxPrincipal = new QCheckBox("Principal",this);

    m_checkboxMonthlyTotal->setChecked(false);
    m_checkboxMonthlyInterestTotal->setChecked(false);
    m_checkboxMonthlyInterestCumul->setChecked(true);
    m_checkboxPrincipal->setChecked(true);

    double dMTot = m_checkboxMonthlyTotal->isChecked()?1.0:0.0;
    double dMIT = m_checkboxMonthlyInterestTotal->isChecked()?1.0:0.0;
    double dMIC = m_checkboxMonthlyInterestCumul->isChecked()?1.0:0.0;
    double dPrinc = m_checkboxPrincipal->isChecked()?1.0:0.0;

    // Compute the default compount interests
    calculateCompoundInterest(m_yearlyInterestPct, m_numberOfYears, m_monthlyAdd, m_startVal, m_quarterlyTotal,
                                   m_quarterlyInterestTotal, m_quarterlyInterestCumul);


    // Set the bar graph
    m_quarterlyTotalSet = new QtCharts::QBarSet("Total Value");
    m_quarterlyInterestSet = new QtCharts::QBarSet("Monthly Interest");
    m_quarterlyInterestCumulSet = new QtCharts::QBarSet("Cumulated Interest");
    m_quarterlyPrincipalSet = new QtCharts::QBarSet("Principal");

    for(int i=0; i<m_numberOfYears*4; i++){
        m_quarterlyTotalSet->append(m_quarterlyTotal[i]*dMTot);
        m_quarterlyInterestSet->append(m_quarterlyInterestTotal[i]*dMIT);
        m_quarterlyInterestCumulSet->append(m_quarterlyInterestCumul[i]*dMIC);
        m_quarterlyPrincipalSet->append((m_quarterlyTotal[i]-m_quarterlyInterestCumul[i])*dPrinc);
    }

    m_QStackedBarSeries = new QtCharts::QStackedBarSeries();
    m_QStackedBarSeries->append(m_quarterlyTotalSet);
    m_QStackedBarSeries->append(m_quarterlyInterestSet);
    m_QStackedBarSeries->append(m_quarterlyPrincipalSet);
    m_QStackedBarSeries->append(m_quarterlyInterestCumulSet);

    m_chart = new QtCharts::QChart();
    m_chart->addSeries(m_QStackedBarSeries);
    m_chart->setTitle("Compound interest chart");
    m_chart->setAnimationOptions(QtCharts::QChart::SeriesAnimations);

    m_axisX = new QtCharts::QValueAxis();
    m_chart->createDefaultAxes();
    m_chart->setAxisX(m_axisX, m_QStackedBarSeries);

    m_chart->legend()->setVisible(true);
    m_chart->legend()->setAlignment(Qt::AlignBottom);

    m_chartView = new QtCharts::QChartView(m_chart, this);
    m_chartView->setRenderHint(QPainter::Antialiasing);
    //m_chartView->set

    // Init the sliders
     m_yearSlider = new QSlider(Qt::Horizontal,this);
     m_yearSlider->setRange(1, 80);
     m_yearSlider->setSingleStep(1);
     m_yearSlider->setSliderPosition(m_numberOfYears);

     m_monthlyAddSlider = new QSlider(Qt::Horizontal,this);     
     m_monthlyAddSlider->setRange(0, 10000);
     m_monthlyAddSlider->setSingleStep(100);
     m_monthlyAddSlider->setSliderPosition(m_monthlyAdd);

     m_interestSlider = new QSlider(Qt::Horizontal,this);
     m_interestSlider->setRange(-10, 30);
     m_interestSlider->setSingleStep(0.1);
     m_interestSlider->setSliderPosition(m_yearlyInterestPct);

     m_startValSlider = new QSlider(Qt::Horizontal,this);
     m_startValSlider->setRange(0, 1000000);
     m_startValSlider->setSingleStep(500);
     m_startValSlider->setSliderPosition(m_startVal);

     // Init the LCDs
     m_yearLCD = new QLCDNumber(this);
     m_yearLCD->display(m_numberOfYears);
     m_monthlyAddLCD = new QLCDNumber(this);
     m_monthlyAddLCD->display(m_monthlyAdd);
     m_interestLCD = new QLCDNumber(this);
     m_interestLCD->display(m_yearlyInterestPct);
     m_startLCD = new QLCDNumber(this);
     m_startLCD->setDigitCount(7);
     m_startLCD->display(m_startVal);

     m_yearLabel = new QLabel("Years",this);
     m_monthlyAddLabel = new QLabel("Monthly Add",this);
     m_interestLabel = new QLabel("Interest",this);
     m_startLabel = new QLabel("Start amount",this);

     // Init the layouts
    QGridLayout *masterLayout = new QGridLayout;

    QVBoxLayout *sliderLayout = new QVBoxLayout;
    sliderLayout->addWidget(m_yearLabel,Qt::AlignCenter);
    sliderLayout->addWidget(m_yearSlider);
    sliderLayout->addWidget(m_yearLCD);
    sliderLayout->addWidget(m_monthlyAddLabel);
    sliderLayout->addWidget(m_monthlyAddSlider);
    sliderLayout->addWidget(m_monthlyAddLCD);
    sliderLayout->addWidget(m_interestLabel);
    sliderLayout->addWidget(m_interestSlider);
    sliderLayout->addWidget(m_interestLCD);
    sliderLayout->addWidget(m_startLabel);
    sliderLayout->addWidget(m_startValSlider);
    sliderLayout->addWidget(m_startLCD);

    // Connect the sliders to the LCDs
    QObject::connect(m_yearSlider, SIGNAL(valueChanged(int)), m_yearLCD, SLOT(display(int)));
    QObject::connect(m_monthlyAddSlider, SIGNAL(valueChanged(int)), m_monthlyAddLCD, SLOT(display(int)));
    QObject::connect(m_interestSlider, SIGNAL(valueChanged(int)), m_interestLCD, SLOT(display(int)));
    QObject::connect(m_startValSlider, SIGNAL(valueChanged(int)), m_startLCD, SLOT(display(int)));

    // Connect the check boxes
    QObject::connect(m_checkboxMonthlyTotal,SIGNAL(stateChanged(int)),this,SLOT(updateCheckBoxMTot(int)));
    QObject::connect(m_checkboxMonthlyInterestTotal,SIGNAL(stateChanged(int)),this,SLOT(updateCheckBoxMIT(int)));
    QObject::connect(m_checkboxMonthlyInterestCumul,SIGNAL(stateChanged(int)),this,SLOT(updateCheckBoxMIC(int)));
    QObject::connect(m_checkboxPrincipal,SIGNAL(stateChanged(int)),this,SLOT(updateCheckBoxPrincipal(int)));

    // Other slots
    QObject::connect(m_yearSlider, SIGNAL(sliderReleased()), this, SLOT(updateYears()));
    QObject::connect(m_monthlyAddSlider, SIGNAL(sliderReleased()), this, SLOT(updateMonthlyAdd()));
    QObject::connect(m_interestSlider, SIGNAL(sliderReleased()), this, SLOT(updateInterest()));
    QObject::connect(m_startValSlider, SIGNAL(sliderReleased()), this, SLOT(updateStartVal()));

    QVBoxLayout *checkboxLayout = new QVBoxLayout;
    checkboxLayout->addWidget(m_checkboxMonthlyTotal);
    checkboxLayout->addWidget(m_checkboxMonthlyInterestTotal);
    checkboxLayout->addWidget(m_checkboxMonthlyInterestCumul);
    checkboxLayout->addWidget(m_checkboxPrincipal);

    masterLayout->addLayout(checkboxLayout,1,1,1,1);
    masterLayout->addLayout(sliderLayout,0,1,1,1);
    masterLayout->addWidget(m_chartView,0,0,2,1);
    masterLayout->setColumnStretch(0,4);
    this->setLayout(masterLayout);

    // Display the window
    this->show();
}

void CustomWidget::updateYears(){
    m_numberOfYears = this->m_yearLCD->value();
    updateWidget();
}

void CustomWidget::updateMonthlyAdd(){
    m_monthlyAdd = this->m_monthlyAddLCD->value();
    updateWidget();
}

void CustomWidget::updateInterest(){
    m_yearlyInterestPct = this->m_interestLCD->value();
    updateWidget();
}

void CustomWidget::updateStartVal(){
    m_startVal = this->m_startLCD->value();
    updateWidget();
}

void CustomWidget::updateCheckBoxMTot(const int s){
    updateWidget();
}

void CustomWidget::updateCheckBoxMIT(const int s){
    updateWidget();
}

void CustomWidget::updateCheckBoxMIC(const int s){
    updateWidget();
}

void CustomWidget::updateCheckBoxPrincipal(const int s){
    updateWidget();
}

void CustomWidget::initLayout() {

}

void CustomWidget::updateWidget() {
    m_quarterlyTotal.resize(m_numberOfYears * 4);
    m_quarterlyInterestTotal.resize(m_numberOfYears * 4);
    m_quarterlyInterestCumul.resize(m_numberOfYears * 4);

    double dMTot = m_checkboxMonthlyTotal->isChecked()?1.0:0.0;
    double dMIT = m_checkboxMonthlyInterestTotal->isChecked()?1.0:0.0;
    double dMIC = m_checkboxMonthlyInterestCumul->isChecked()?1.0:0.0;
    double dPrinc = m_checkboxPrincipal->isChecked()?1.0:0.0;

    calculateCompoundInterest(m_yearlyInterestPct, m_numberOfYears, m_monthlyAdd, m_startVal, m_quarterlyTotal,
                                   m_quarterlyInterestTotal, m_quarterlyInterestCumul);

    // Set the bar graph
    m_quarterlyTotalSet = new QtCharts::QBarSet("Total Value");
    m_quarterlyInterestSet = new QtCharts::QBarSet("Monthly Interest");
    m_quarterlyInterestCumulSet = new QtCharts::QBarSet("Cumulated Interest");
    m_quarterlyPrincipalSet = new QtCharts::QBarSet("Principal");

    for(int i=0; i<m_numberOfYears*4; i++){
        m_quarterlyTotalSet->append(m_quarterlyTotal[i]*dMTot);
        m_quarterlyInterestSet->append(m_quarterlyInterestTotal[i]*dMIT);
        m_quarterlyInterestCumulSet->append(m_quarterlyInterestCumul[i]*dMIC);
        m_quarterlyPrincipalSet->append((m_quarterlyTotal[i]-m_quarterlyInterestCumul[i])*dPrinc);
    }

    m_QStackedBarSeries->clear();
    m_QStackedBarSeries->append(m_quarterlyTotalSet);
    m_QStackedBarSeries->append(m_quarterlyInterestSet);
    m_QStackedBarSeries->append(m_quarterlyPrincipalSet);
    m_QStackedBarSeries->append(m_quarterlyInterestCumulSet);

    std::valarray<double> maxVal = m_quarterlyTotal*dMTot + m_quarterlyInterestTotal*dMIT + m_quarterlyInterestCumul*dMIC + (m_quarterlyTotal-m_quarterlyInterestCumul)*dPrinc;

    m_axisX = new QtCharts::QValueAxis();
    m_axisX->setRange(1,m_numberOfYears * 4);
    m_axisY = new QtCharts::QValueAxis();
    m_axisY->setRange(0,maxVal.max());
    m_chart->createDefaultAxes();
    m_chart->setAxisX(m_axisX, m_QStackedBarSeries);
    m_chart->setAxisY(m_axisY, m_QStackedBarSeries);

    // Display the window
    this->show();
}

void CustomWidget::calculateCompoundInterest(double yearlyInterestPct, int numberOfYears, double monthlyAdd, double startVal, std::valarray<double> &quarterlyTotal,
                               std::valarray<double> &quarterlyInterestTotal, std::valarray<double> &quarterlyInterestCumul) {

    int idx = 0;
    double quarterlyInterest = 1 + yearlyInterestPct / 4 / 100;// convert from percent to 1.X

    quarterlyTotal[0] = startVal;

    for (int yearIdx = 0; yearIdx < numberOfYears; yearIdx++) {
        for (int quarterIdx = 0; quarterIdx < 4; quarterIdx++) {
            idx = yearIdx * 4 + quarterIdx;

            if (idx > 0) {
                quarterlyTotal[idx] = quarterlyTotal[idx - 1] * quarterlyInterest + 4*monthlyAdd;
                quarterlyInterestTotal[idx] = yearlyInterestPct / 4 / 100 * quarterlyTotal[idx - 1];
                quarterlyInterestCumul[idx] = quarterlyInterestCumul[idx - 1] + quarterlyInterestTotal[idx];
            }
        }
    }
}

CustomWidget::~CustomWidget(){

}
