/****************************************************************************
** Meta object code from reading C++ file 'CustomWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../FinSim/CustomWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CustomWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CustomWidget_t {
    QByteArrayData data[10];
    char stringdata0[152];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CustomWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CustomWidget_t qt_meta_stringdata_CustomWidget = {
    {
QT_MOC_LITERAL(0, 0, 12), // "CustomWidget"
QT_MOC_LITERAL(1, 13, 11), // "updateYears"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 16), // "updateMonthlyAdd"
QT_MOC_LITERAL(4, 43, 14), // "updateInterest"
QT_MOC_LITERAL(5, 58, 14), // "updateStartVal"
QT_MOC_LITERAL(6, 73, 18), // "updateCheckBoxMTot"
QT_MOC_LITERAL(7, 92, 17), // "updateCheckBoxMIT"
QT_MOC_LITERAL(8, 110, 17), // "updateCheckBoxMIC"
QT_MOC_LITERAL(9, 128, 23) // "updateCheckBoxPrincipal"

    },
    "CustomWidget\0updateYears\0\0updateMonthlyAdd\0"
    "updateInterest\0updateStartVal\0"
    "updateCheckBoxMTot\0updateCheckBoxMIT\0"
    "updateCheckBoxMIC\0updateCheckBoxPrincipal"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CustomWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x0a /* Public */,
       3,    0,   55,    2, 0x0a /* Public */,
       4,    0,   56,    2, 0x0a /* Public */,
       5,    0,   57,    2, 0x0a /* Public */,
       6,    1,   58,    2, 0x0a /* Public */,
       7,    1,   61,    2, 0x0a /* Public */,
       8,    1,   64,    2, 0x0a /* Public */,
       9,    1,   67,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,

       0        // eod
};

void CustomWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CustomWidget *_t = static_cast<CustomWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->updateYears(); break;
        case 1: _t->updateMonthlyAdd(); break;
        case 2: _t->updateInterest(); break;
        case 3: _t->updateStartVal(); break;
        case 4: _t->updateCheckBoxMTot((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->updateCheckBoxMIT((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->updateCheckBoxMIC((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->updateCheckBoxPrincipal((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject CustomWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CustomWidget.data,
      qt_meta_data_CustomWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CustomWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CustomWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CustomWidget.stringdata0))
        return static_cast<void*>(const_cast< CustomWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int CustomWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
